import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from '@angular/forms';

export function matchValue(controToCheck: AbstractControl): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const toCheckControlValue = controToCheck.value;
    const doesNotMatch = toCheckControlValue !== control.value;
    return doesNotMatch ? {'matchValue': {value: control.value}} : null;
  };
}

@Directive({
  selector: '[appMatchValue]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: MatchValueValidatorDirective,
    multi: true
  }]
})
export class MatchValueValidatorDirective implements Validator {
  @Input()
  value: AbstractControl;

  validate(c: AbstractControl): ValidationErrors | any {
    return this.value ?
      matchValue(this.value)(c) :
      null;
  }
}
