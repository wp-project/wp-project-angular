import { HomeComponent } from '../components/home/home.component';
import { AuthGuard } from '../guards/auth/auth.guard';
import { LoginComponent } from '../components/login/login.component';
import { RegisterComponent } from '../components/register/register.component';
import { LoggedInGuard } from '../guards/logged-in/logged-in.guard';
import { QuizComponent } from '../components/quiz/quiz.component';
import { QuizGuard } from '../guards/quiz/quiz.guard';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { PlayerComponent } from '../components/player/player.component';
import { SearchComponent } from '../components/search/search.component';
import { FriendRequestsComponent } from '../components/friend-requests/friend-requests.component';
import { FindPlayersComponent } from '../components/find-players/find-players.component';
import { OopsComponent } from '../components/oops/oops.component';
import { RateFriendComponent } from '../components/rate-friend/rate-friend.component';
import { EditComponent } from '../components/edit/edit.component';
import { FriendListComponent } from '../components/friend-list/friend-list.component';

export const ROUTES = [
  {
    path: '',
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'user/:username',
        component: PlayerComponent
      },
      {
        path: 'search',
        component: SearchComponent
      },
      {
        path: 'friend-requests',
        component: FriendRequestsComponent
      },
      {
        path: 'find',
        component: FindPlayersComponent
      },
      {
        path: 'rate/:username',
        component: RateFriendComponent
      },
      {
        path: 'friends',
        component: FriendListComponent
      },
      {
        path: 'edit',
        component: EditComponent
      },
      {
        path: '',
        canActivateChild: [QuizGuard],
        children: [
          {
            path: 'quiz',
            component: QuizComponent
          }
        ]
      }
    ]
  },
  {
    path: '',
    canActivateChild: [LoggedInGuard],
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      }
    ]
  },
  {
    path: 'oops',
    component: OopsComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
