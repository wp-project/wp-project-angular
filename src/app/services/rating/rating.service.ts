import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {QuizAnswers} from '../../models/quiz-answers';
import {Observable} from 'rxjs/Observable';
import {Rating} from '../../models/rating';
import { environment } from '../../../environments/environment';

@Injectable()
export class RatingService {
  private baseUrl = `${environment.apiUrl}/api/ratings`;

  constructor(private http: HttpClient) {
  }

  submitQuizAnswers(quizAnswers: QuizAnswers): Observable<any> {
    return this.http.post(`${this.baseUrl}/quiz`, quizAnswers);
  }

  getRatingForPlayer(username: string): Observable<Rating> {
    return this.http.get<Rating>(`${this.baseUrl}/byUser?username=${username}`);
  }

  isRated(username: string): Observable<boolean> {
    return this.http.get<boolean>(`${this.baseUrl}/isRated?username=${username}`);
  }

  ratePlayer(username: string, rating: Rating): Observable<any> {
    return this.http.post(`${this.baseUrl}?username=${username}`, rating);
  }
}
