import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { FriendRequest } from '../../models/friend-request';
import { environment } from '../../../environments/environment';
import { Player } from '../../models/player';

@Injectable()
export class FriendService {

  private baseUrl = `${environment.apiUrl}/api/friends`;

  constructor(private http: HttpClient) { }

  isFriend(username: string): Observable<boolean> {
    return this.http.get<boolean>(`${this.baseUrl}/isFriend?username=${username}`);
  }

  hasFriendRequest(username: string): Observable<boolean> {
    return this.http.get<boolean>(`${this.baseUrl}/hasFriendRequest?username=${username}`);
  }

  addFriend(username: string): Observable<boolean> {
    return this.http.post<boolean>(`${this.baseUrl}/addFriend?username=${username}`, {});
  }

  getUnconfirmedFriendRequestCount(): Observable<number> {
    return this.http.get<number>(`${this.baseUrl}/friendRequests`);
  }

  getFriendRequests(): Observable<FriendRequest[]> {
    return this.http.get<FriendRequest[]>(`${this.baseUrl}/friendRequestsFull`);
  }

  resolveFriendRequest(id: number, resolution: boolean): any {
    return this.http.get(`${this.baseUrl}/resolveRequest/${id}?resolution=${resolution}`);
  }

  getFriends(): Observable<Player[]> {
    return this.http.get<Player[]>(this.baseUrl);
  }
}
