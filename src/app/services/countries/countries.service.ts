import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CountriesService {
  private baseUrl = 'https://restcountries.eu/rest/v2/';

  constructor(private http: HttpClient) {
  }

  getAllCountries(): Observable<any> {
    return this.http.get(this.baseUrl + 'all');
  }
}
