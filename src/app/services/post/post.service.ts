import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../../models/post';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Injectable()
export class PostService {
  private baseUrl = `${environment.apiUrl}/api/posts`;

  constructor(private http: HttpClient) {
  }

  newPost(post: Post): Observable<Post> {
    return this.http.post<Post>(this.baseUrl, post);
  }

  getPostsForPlayer(username: string): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.baseUrl}?username=${username}`);
  }

  getFriendPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.baseUrl}/friends`);
  }
}
