import {Injectable} from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() {
  }

  hasItem(key: string): boolean {
    return localStorage.getItem(key) != null;
  }

  getItem(key: string): string {
    return this.hasItem(key) ? localStorage.getItem(key) : null;
  }

  setItem(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  removeItem(key: string) {
    if (this.hasItem(key)) {
      localStorage.removeItem(key);
    }
  }
}
