import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/of';
import { Player } from '../../models/player';
import { LocalStorageService } from '../local-storage/local-storage.service';
import { LoggedInPlayer } from '../../models/logged-in-player';
import { environment } from '../../../environments/environment';
import { PlayerQuery } from '../../models/player-query';
import { MatchedPlayer } from '../../models/matched-player';

@Injectable()
export class UserService {

  private baseUrl = environment.apiUrl;

  constructor(private http: HttpClient,
    private localStorageService: LocalStorageService) {
  }

  isLoggedIn(): boolean {
    return this.localStorageService.hasItem('username');
  }

  attemptLogin(username: string, password: string): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');

    const body = 'username=' + username + '&password=' + password;

    return this.http.post(this.baseUrl + '/login', body, { headers: headers });
  }

  logout(): Observable<any> {
    return this.http.post(this.baseUrl + '/logout', {});
  }

  userLoggedOut() {
    this.localStorageService.removeItem('username');
    this.localStorageService.removeItem('friendRequests');
  }

  registerUser(player: Player): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.baseUrl}/register`, player, { headers: headers });
  }

  getAuthenticatedPlayer(): Observable<LoggedInPlayer> {
    return this.http.get<LoggedInPlayer>(`${this.baseUrl}/loggedInUser`);
  }

  findUsers(term: string): Observable<Player[]> {
    return this.http.get<Player[]>(`${this.baseUrl}/api/users/findUsers?term=${term}`);
  }

  findUserByUsername(username: String): Observable<Player> {
    return this.http.get<Player>(`${this.baseUrl}/api/users?username=${username}`);
  }

  findPlayers(playerQuery: PlayerQuery): Observable<MatchedPlayer[]> {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post<MatchedPlayer[]>(`${this.baseUrl}/api/users/matchPlayers`, playerQuery, { headers: headers });
  }

  editPlayer(player: Player): Observable<any> {
    return this.http.patch(`${this.baseUrl}/api/users`, player);
  }
}
