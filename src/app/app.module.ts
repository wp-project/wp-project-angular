import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './components/app/app.component';
import { HomeComponent } from './components/home/home.component';
import { ROUTES } from './routing/routes';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth/auth.guard';
import { UserService } from './services/user/user.service';
import { LoginComponent } from './components/login/login.component';
import { AlertModule, BsDropdownModule, TooltipModule } from 'ngx-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { RegisterComponent } from './components/register/register.component';
import { LocalStorageService } from './services/local-storage/local-storage.service';
import { CountriesService } from './services/countries/countries.service';
import { LoggedInGuard } from './guards/logged-in/logged-in.guard';
import { NouisliderModule } from 'ng2-nouislider';
import { QuizComponent } from './components/quiz/quiz.component';
import { QuizQuestionComponent } from './components/quiz-question/quiz-question.component';
import { RatingService } from './services/rating/rating.service';
import { QuizGuard } from './guards/quiz/quiz.guard';
import { PlayerRatingComponent } from './components/player-rating/player-rating.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RatingValueComponent } from './components/rating-value/rating-value.component';
import { CommonModule } from '@angular/common';
import { NewPostComponent } from './components/new-post/new-post.component';
import { PostService } from './services/post/post.service';
import { PostComponent } from './components/post/post.component';
import { PostListComponent } from './components/post-list/post-list.component';
import { PlayerComponent } from './components/player/player.component';
import { SearchComponent } from './components/search/search.component';
import { PlayerCompactComponent } from './components/player-compact/player-compact.component';
import { PlayerInteractionComponent } from './components/player-interaction/player-interaction.component';
import { FriendService } from './services/friend/friend.service';
import { FriendNewsComponent } from './components/friend-news/friend-news.component';
import { FriendRequestsComponent } from './components/friend-requests/friend-requests.component';
import { FriendRequestCompactComponent } from './components/friend-request-compact/friend-request-compact.component';
import { FindPlayersComponent } from './components/find-players/find-players.component';
import { OopsComponent } from './components/oops/oops.component';
import { RateFriendComponent } from './components/rate-friend/rate-friend.component';
import { EditComponent } from './components/edit/edit.component';
import { FriendListComponent } from './components/friend-list/friend-list.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    QuizComponent,
    QuizQuestionComponent,
    PlayerRatingComponent,
    NotFoundComponent,
    RatingValueComponent,
    NewPostComponent,
    PostComponent,
    PostListComponent,
    PlayerComponent,
    SearchComponent,
    PlayerCompactComponent,
    PlayerInteractionComponent,
    FriendNewsComponent,
    FriendRequestsComponent,
    FriendRequestCompactComponent,
    FindPlayersComponent,
    OopsComponent,
    RateFriendComponent,
    EditComponent,
    FriendListComponent
  ],
  imports: [
    FormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    AlertModule.forRoot(),
    NouisliderModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    AuthGuard,
    QuizGuard,
    LoggedInGuard,
    UserService,
    PostService,
    RatingService,
    FriendService,
    CountriesService,
    LocalStorageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
