import { Component, OnInit, Input } from '@angular/core';
import { FriendService } from '../../services/friend/friend.service';
import { FriendRequest } from '../../models/friend-request';
import { LocalStorageService } from '../../services/local-storage/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-friend-requests',
  templateUrl: './friend-requests.component.html',
  styleUrls: ['./friend-requests.component.css']
})
export class FriendRequestsComponent implements OnInit {
  @Input()
  friendRequests: FriendRequest[];

  constructor(private friendService: FriendService,
    private localStorageService: LocalStorageService,
    private router: Router) { }

  ngOnInit() {
    this.friendService.getFriendRequests().subscribe(res => {
      this.friendRequests = res;
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

  requestResolved(id: number) {
    this.friendRequests.splice(this.friendRequests.findIndex(value => value.id === id));
    this.localStorageService.setItem('friendRequests', (+this.localStorageService.getItem('friendRequests') - 1) + '');
  }
}
