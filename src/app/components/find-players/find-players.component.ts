import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CountriesService } from '../../services/countries/countries.service';
import { PlayerQuery } from '../../models/player-query';
import { UserService } from '../../services/user/user.service';
import { MatchedPlayer } from '../../models/matched-player';
import { Router } from '@angular/router';

@Component({
  selector: 'app-find-players',
  templateUrl: './find-players.component.html',
  styleUrls: ['./find-players.component.css']
})
export class FindPlayersComponent implements OnInit {

  findPlayersForm: FormGroup;
  hasResult: boolean;
  countries: string[];
  matchedPlayers: MatchedPlayer[];

  constructor(private formBuilder: FormBuilder,
    private countriesService: CountriesService,
    private userService: UserService,
    private router: Router) {
    this.createForm();
  }

  ngOnInit() {
    this.hasResult = false;
    this.countriesService.getAllCountries().subscribe(res => {
      this.countries = res.map(el => el.name);
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

  createForm() {
    this.findPlayersForm = this.formBuilder.group({
      solo: 0,
      skill: 0,
      speech: 0,
      spirit: 0,
      sophistication: 0,
      country: ''
    });
  }

  findPlayers() {
    const playerQuery = this.preparePlayerQuery();

    this.userService.findPlayers(playerQuery).subscribe(players => {
      if (players.length !== 0) {
        this.matchedPlayers = players;
        this.hasResult = true;
      }
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

  preparePlayerQuery(): PlayerQuery {

    const playerQuery = new PlayerQuery();

    playerQuery.solo = this.findPlayersForm.controls['solo'].value;
    playerQuery.skill = this.findPlayersForm.controls['skill'].value;
    playerQuery.speech = this.findPlayersForm.controls['speech'].value;
    playerQuery.spirit = this.findPlayersForm.controls['spirit'].value;
    playerQuery.sophistication = this.findPlayersForm.controls['sophistication'].value;
    playerQuery.country = this.findPlayersForm.controls['country'].value;

    return playerQuery;
  }

  viewProfile(username: string) {
    this.router.navigate(['/user', username]);
  }

  findAgain() {
    this.hasResult = false;
    this.matchedPlayers = MatchedPlayer[0];
  }
}
