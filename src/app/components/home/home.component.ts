import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { Router } from '@angular/router';
import { RatingService } from '../../services/rating/rating.service';
import { LocalStorageService } from '../../services/local-storage/local-storage.service';
import { Rating } from '../../models/rating';
import { Post } from '../../models/post';
import { PostService } from '../../services/post/post.service';
import { FriendService } from '../../services/friend/friend.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'app';
  rating: Rating;
  posts = [];

  constructor(private userService: UserService,
    private router: Router,
    private ratingService: RatingService,
    private friendService: FriendService,
    private localStorageService: LocalStorageService,
    private postService: PostService) {
  }

  ngOnInit() {
    this.getPlayerRating();
    this.getPlayerPosts();
    this.getUnconfirmedFriendRequests();
  }

  private getPlayerRating() {
    this.ratingService.getRatingForPlayer(this.localStorageService.getItem('username')).subscribe(rating => {
      this.rating = rating;
    }, err => {
    });
  }

  private getPlayerPosts() {
    this.postService.getPostsForPlayer(this.localStorageService.getItem('username')).subscribe(posts => {
      this.posts = posts;
    }, err => {
    });
  }

  logout() {
    this.userService.logout().subscribe(res => {
      this.userService.userLoggedOut();
      this.router.navigate(['/login']);
    });
  }

  addPost(event: Post) {
    this.posts.unshift(event);
  }

  getUnconfirmedFriendRequests() {
    this.friendService.getUnconfirmedFriendRequestCount().subscribe(res => {
      this.localStorageService.setItem('friendRequests', res + '');
    }, err => {
    });
  }
}
