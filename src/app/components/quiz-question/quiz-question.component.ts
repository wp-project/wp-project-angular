import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-quiz-question',
  templateUrl: './quiz-question.component.html',
  styleUrls: ['./quiz-question.component.css']
})
export class QuizQuestionComponent implements OnInit {
  @Input()
  question: string;
  @Input()
  leftSideText: string;
  @Input()
  rightSideText: string;
  @Input()
  min: number;
  @Input()
  max: number;
  @Input()
  step: number;
  @Input()
  control: FormControl;

  constructor() {
  }

  ngOnInit() {
  }

}
