import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { Router } from '@angular/router';
import { LocalStorageService } from '../../services/local-storage/local-storage.service';
import { FriendService } from '../../services/friend/friend.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private searchTerm = '';
  private year: number;

  constructor(private localStorageService: LocalStorageService,
    private userService: UserService,
    private friendService: FriendService,
    private router: Router) {
      this.year = new Date().getFullYear();
  }

  logout() {
    this.userService.logout().subscribe(res => {
      this.userService.userLoggedOut();
      this.router.navigate(['/login']);
    });
  }

  isLoggedIn() {
    return this.userService.isLoggedIn();
  }

  takeQuiz() {
    this.router.navigate(['/quiz']);
  }

  keyPressed(event) {
    if (event['key'] === 'Enter') {
      this.searchForPlayers();
    }
  }

  searchForPlayers() {
    if (this.searchTerm == null || this.searchTerm === '') {
      return;
    }
    this.router.navigate(['/search'], {
      queryParams: {
        term: this.searchTerm
      }
    });
    this.searchTerm = '';
  }

  viewFriendRequests() {
    this.router.navigate(['/friend-requests']);
  }
}
