import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../services/user/user.service';
import { CountriesService } from '../../services/countries/countries.service';
import { LocalStorageService } from '../../services/local-storage/local-storage.service';
import { Player } from '../../models/player';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  private playerForm: FormGroup;
  private countries: String[];
  private player: Player;

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private countryService: CountriesService,
    private localStorageService: LocalStorageService,
    private router: Router) {
  }

  ngOnInit() {

    const username = this.localStorageService.getItem('username');
    this.countryService.getAllCountries().subscribe(res => {
      this.countries = res.map(country => country.name);
    });
    this.userService.findUserByUsername(username).subscribe(player => {
      this.player = player;
      this.createForm();
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

  createForm() {
    this.playerForm = this.formBuilder.group({
      nickname: [this.player.nickname, [
        Validators.minLength(2),
        Validators.maxLength(20),
        Validators.pattern(/^[a-zA-Z0-9]*$/)]],
      country: [this.player.country],
      steamid: [this.player.steamId],
      battleTag: [this.player.battleTag, [
        Validators.pattern(/^.*#\d.*$/)
      ]],
      originid: [this.player.originId]
    });
  }

  edit() {
    this.player.nickname = this.playerForm.controls.nickname.value;
    this.player.country = this.playerForm.controls.country.value === '' ? this.player.country : this.playerForm.controls.country.value;
    this.player.steamId = this.playerForm.controls.steamid.value;
    this.player.battleTag = this.playerForm.controls.battleTag.value;
    this.player.originId = this.playerForm.controls.originid.value;

    this.userService.editPlayer(this.player).subscribe(res => {
      this.localStorageService.setItem('nickname', this.player.nickname);
      this.router.navigate(['/home']);
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

}
