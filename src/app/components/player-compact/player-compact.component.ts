import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Player } from '../../models/player';

@Component({
  selector: 'app-player-compact',
  templateUrl: './player-compact.component.html',
  styleUrls: ['./player-compact.component.css']
})
export class PlayerCompactComponent implements OnInit {

  @Input()
  private player: Player;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  redirectToPlayer() {
    this.router.navigate(['/user', this.player.username]);
  }

  getSteamProfileLink(): string {
    return `https://steamcommunity.com/profiles/${this.player.steamId}`;
  }

}
