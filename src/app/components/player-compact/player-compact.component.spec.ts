import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerCompactComponent } from './player-compact.component';

describe('PlayerCompactComponent', () => {
  let component: PlayerCompactComponent;
  let fixture: ComponentFixture<PlayerCompactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerCompactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerCompactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
