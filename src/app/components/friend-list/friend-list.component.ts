import { Component, OnInit } from '@angular/core';
import { FriendService } from '../../services/friend/friend.service';
import { Player } from '../../models/player';
import { Router } from '@angular/router';

@Component({
  selector: 'app-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.css']
})
export class FriendListComponent implements OnInit {

  private friends: Player[];

  constructor(private friendService: FriendService,
    private router: Router) { }

  ngOnInit() {
    this.friendService.getFriends().subscribe(friends => {
      console.log(friends);
      this.friends = friends;
    }, err => {
      this.router.navigate(['/oops']);
    })
  }

}
