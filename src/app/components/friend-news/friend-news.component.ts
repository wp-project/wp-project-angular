import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PostService } from '../../services/post/post.service';
import { Post } from '../../models/post';

@Component({
  selector: 'app-friend-news',
  templateUrl: './friend-news.component.html',
  styleUrls: ['./friend-news.component.css']
})
export class FriendNewsComponent implements OnInit {

  posts = Post[0];

  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.getFriendPosts().subscribe(posts => {
      this.posts = posts;
    }, err => {
    });
  }

}
