import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Player} from '../../models/player';
import {UserService} from '../../services/user/user.service';
import {Router} from '@angular/router';
import {matchValue} from '../../validators/match-value.directive';
import {CountriesService} from '../../services/countries/countries.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  error = '';
  countries = [];
  playerForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private countriesService: CountriesService,
              private router: Router) {
    this.createForm();
  }

  ngOnInit() {
    this.populateCountriesList();
  }

  createForm() {
    this.playerForm = this.formBuilder.group({
      username: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20),
        Validators.pattern('^[a-zA-Z0-9]*$')]],
      email: ['', [
        Validators.required,
        Validators.email]],
      emailConfirm: [''],
      password: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20),
        Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.,$!@#])/)]],
      passwordConfirm: [''],
      nickname: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20),
        Validators.pattern('^[a-zA-Z0-9]*$')]],
      country: ['', Validators.required],
      steamid: [''],
      battleTag: ['', [
        Validators.pattern(/^.*#\d.*$/)
      ]],
      originid: ['']
    });

    this.playerForm.get('emailConfirm').setValidators([
      Validators.required,
      matchValue(this.playerForm.get('email'))
    ]);

    this.playerForm.get('passwordConfirm').setValidators([
      Validators.required,
      matchValue(this.playerForm.get('password'))
    ]);
  }

  register() {
    const player = new Player();
    player.username = this.playerForm.get('username').value;
    player.email = this.playerForm.get('email').value;
    player.password = this.playerForm.get('password').value;
    player.nickname = this.playerForm.get('nickname').value;
    player.country = this.playerForm.get('country').value;
    player.steamId = this.playerForm.get('steamid').value;
    player.battleTag = this.playerForm.get('battleTag').value;
    player.originId = this.playerForm.get('originid').value;

    this.userService.registerUser(player).subscribe(response => {
      this.router.navigate(['/login']);
    }, error => {
      this.error = 'Username already exists.';
    });
  }

  private populateCountriesList() {
    this.countriesService.getAllCountries().subscribe(result => {
      this.countries = result.map(res => res.name);
    }, err => {
      this.router.navigate(['/oops']);
    });
  }
}
