import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Player } from '../../models/player';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  private players: Player[];

  constructor(private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {

    this.activatedRoute.queryParams.subscribe(res => {
      this.userService.findUsers(res['term']).subscribe(searchResults => {
        this.players = searchResults;
      }, searchError => {
        this.router.navigate(['/oops']);
      });
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

}
