import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Post } from '../../models/post';
import { LocalStorageService } from '../../services/local-storage/local-storage.service';
import { PostService } from '../../services/post/post.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
  postForm: FormGroup;
  @Output()
  newPostEvent = new EventEmitter<Post>();

  constructor(private formBuilder: FormBuilder,
    private localStorageService: LocalStorageService,
    private postService: PostService,
    private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.postForm = this.formBuilder.group({
      text: ['', [
        Validators.required,
        Validators.maxLength(160),
        Validators.minLength(2)
      ]],
      link: ['', [
        Validators.pattern(/^http(.*\.(jpg|jpeg|png|gif).*)/)
      ]]
    });
  }

  post() {
    const post = new Post();
    post.username = this.localStorageService.getItem('username');
    post.text = this.postForm.get('text').value;
    post.link = this.postForm.get('link').value;

    this.postService.newPost(post).subscribe(succ => {
      this.postForm.reset();
      this.newPostEvent.emit(succ);
    }, err => {
      this.router.navigate(['/oops']);
    });
  }
}
