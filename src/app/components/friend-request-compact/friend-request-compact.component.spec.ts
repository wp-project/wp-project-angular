import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendRequestCompactComponent } from './friend-request-compact.component';

describe('FriendRequestCompactComponent', () => {
  let component: FriendRequestCompactComponent;
  let fixture: ComponentFixture<FriendRequestCompactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FriendRequestCompactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendRequestCompactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
