import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FriendRequest } from '../../models/friend-request';
import { FriendService } from '../../services/friend/friend.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-friend-request-compact',
  templateUrl: './friend-request-compact.component.html',
  styleUrls: ['./friend-request-compact.component.css']
})
export class FriendRequestCompactComponent implements OnInit {
  @Input()
  friendRequest: FriendRequest;
  @Output()
  requestResolved: EventEmitter<number> = new EventEmitter<number>();
  private resolved: boolean;

  constructor(private friendService: FriendService,
    private router: Router) { }

  ngOnInit() {
    this.resolved = false;
  }

  resolve(resolution: boolean) {
    this.friendService.resolveFriendRequest(this.friendRequest.id, resolution).subscribe(res => {
      this.resolved = true;
      this.requestResolved.emit(this.friendRequest.id);
    }, err => {
      this.router.navigate(['/oops']);
    });
  }
}
