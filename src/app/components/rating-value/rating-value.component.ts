import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-rating-value',
  templateUrl: './rating-value.component.html',
  styleUrls: ['./rating-value.component.css']
})
export class RatingValueComponent implements OnInit {
  @Input()
  value: number;
  @Input()
  statName: string;
  negativeWidth = '0%';
  positiveWidth = '0%';

  constructor() {
  }

  ngOnInit() {
    if (this.value > 0) {
      this.positiveWidth = `${this.value * 20}%`;
    } else {
      this.negativeWidth = `${-this.value * 20}%`;
    }
  }

}
