import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RatingService } from '../../services/rating/rating.service';
import { Rating } from '../../models/rating';
import { Post } from '../../models/post';
import { PostService } from '../../services/post/post.service';
import { LocalStorageService } from '../../services/local-storage/local-storage.service';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  private userExists: boolean;
  private username: string;
  private rating: Rating;
  private posts: Post[];

  constructor(private activatedRoute: ActivatedRoute,
    private ratingService: RatingService,
    private postService: PostService,
    private localStorageService: LocalStorageService,
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
    this.userExists = true;

    this.activatedRoute.params.subscribe(params => {
      this.username = params['username'];
      if (this.username === this.localStorageService.getItem('username')) {
        this.router.navigate(['/home']);
      }

      this.userService.findUserByUsername(this.username).subscribe(res => {

        this.ratingService.getRatingForPlayer(this.username).subscribe(rating => {
          this.rating = rating;
        }, err => {
        });

        this.postService.getPostsForPlayer(this.username).subscribe(posts => {
          this.posts = posts;
        }, err => {
        });

      }, err => {
        this.userExists = false;
      });
    });
  }
}
