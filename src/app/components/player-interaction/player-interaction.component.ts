import { Component, OnInit, Input } from '@angular/core';
import { FriendService } from '../../services/friend/friend.service';
import { RatingService } from '../../services/rating/rating.service';
import { UserService } from '../../services/user/user.service';
import { Player } from '../../models/player';
import { Router } from '@angular/router';

@Component({
  selector: 'app-player-interaction',
  templateUrl: './player-interaction.component.html',
  styleUrls: ['./player-interaction.component.css']
})
export class PlayerInteractionComponent implements OnInit {
  @Input()
  username: string;
  player: Player;
  isFriend: boolean;
  isRated: boolean;
  hasFriendRequest: boolean;

  constructor(private friendService: FriendService,
    private ratingService: RatingService,
    private userService: UserService,
    private router: Router) {
  }

  ngOnInit() {
    this.getPlayer();
    this.getHasFriendRequest();
    this.getIsFriend();
  }

  private getIsFriend() {
    this.friendService.isFriend(this.username).subscribe(isFriend => {
      this.isFriend = isFriend;
      if (this.isFriend === true) {
        this.getIsRated();
      }
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

  private getIsRated() {
    this.ratingService.isRated(this.username).subscribe(isRated => {
      this.isRated = isRated;
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

  private getPlayer() {
    this.userService.findUserByUsername(this.username).subscribe(player => {
      this.player = player;
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

  getHasFriendRequest() {
    this.friendService.hasFriendRequest(this.username).subscribe(hasFriendRequest => {
      this.hasFriendRequest = hasFriendRequest;
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

  addFriend() {
    this.friendService.addFriend(this.username).subscribe(res => {
      this.hasFriendRequest = true;
    }, err => {
      this.router.navigate(['/oops']);
    });
  }

  rate() {
    this.router.navigate(['/rate', this.username]);
  }

  getSteamProfileLink(): string {
    return `https://steamcommunity.com/profiles/${this.player.steamId}`;
  }
}
