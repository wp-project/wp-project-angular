import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {QuizAnswers} from '../../models/quiz-answers';
import {RatingService} from '../../services/rating/rating.service';
import {Router} from '@angular/router';
import {LocalStorageService} from '../../services/local-storage/local-storage.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {
  private quizForm;

  constructor(private formBuilder: FormBuilder,
              private ratingService: RatingService,
              private router: Router,
              private localStorageService: LocalStorageService) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.quizForm = this.formBuilder.group({
      solo1: 0,
      solo2: 0,
      solo3: 0,
      solo4: 0,
      speech1: 0,
      speech2: 0,
      speech3: 0,
      spirit1: 0,
      spirit2: 0,
      spirit3: 0,
      spirit4: 0,
      soph1: 0,
      soph2: 0,
      soph3: 0,
      soph4: 0,
      soph5: 0,
      soph6: 0
    });
  }

  submitQuiz() {
    const quizAnswers = new QuizAnswers();
    quizAnswers.soloAnswers = [
      this.quizForm.get('solo1').value,
      this.quizForm.get('solo2').value,
      this.quizForm.get('solo3').value,
      this.quizForm.get('solo4').value
    ];
    quizAnswers.speechAnswers = [
      this.quizForm.get('speech1').value,
      this.quizForm.get('speech2').value,
      this.quizForm.get('speech3').value
    ];
    quizAnswers.spiritAnswers = [
      this.quizForm.get('spirit1').value,
      this.quizForm.get('spirit2').value,
      this.quizForm.get('spirit3').value,
      this.quizForm.get('spirit4').value
    ];
    quizAnswers.sophAnswers = [
      this.quizForm.get('soph1').value,
      this.quizForm.get('soph2').value,
      this.quizForm.get('soph3').value,
      this.quizForm.get('soph4').value,
      this.quizForm.get('soph5').value,
      this.quizForm.get('soph6').value
    ];
    this.ratingService.submitQuizAnswers(quizAnswers).subscribe(response => {
      this.localStorageService.setItem('hasCompletedQuiz', 'true');
      this.router.navigate(['/home']);
    }, err => {
      this.router.navigate(['/oops']);
    });
  }
}
