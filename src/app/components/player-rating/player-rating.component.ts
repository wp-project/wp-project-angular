import {Component, Input, OnInit} from '@angular/core';
import {Rating} from '../../models/rating';

@Component({
  selector: 'app-player-rating',
  templateUrl: './player-rating.component.html',
  styleUrls: ['./player-rating.component.css']
})
export class PlayerRatingComponent implements OnInit {
  @Input()
  rating: Rating;

  constructor() {
  }

  ngOnInit() {
  }

}
