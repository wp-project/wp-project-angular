import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../models/post';
import { DomSanitizer } from '@angular/platform-browser';
import { LocalStorageService } from '../../services/local-storage/local-storage.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  @Input()
  post: Post;

  constructor(private sanitizer: DomSanitizer,
    private localStorageService: LocalStorageService) { }

  ngOnInit() {
    // TODO: Get comments for post
  }

  isImage(): boolean {
    if (this.post == null || this.post.link == null) {
      return false;
    }

    if (/^http.*\.(jpg|jpeg|png|gif).*/.test(this.post.link)) {
      return true;
    }

    return false;
  }

  getUrl() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.post.link);
  }
}
