import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateFriendComponent } from './rate-friend.component';

describe('RateFriendComponent', () => {
  let component: RateFriendComponent;
  let fixture: ComponentFixture<RateFriendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateFriendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateFriendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
