import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from '../../services/local-storage/local-storage.service';
import { FriendService } from '../../services/friend/friend.service';
import { RatingService } from '../../services/rating/rating.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Rating } from '../../models/rating';

@Component({
  selector: 'app-rate-friend',
  templateUrl: './rate-friend.component.html',
  styleUrls: ['./rate-friend.component.css']
})
export class RateFriendComponent implements OnInit {

  private username: string;
  private rateForm: FormGroup;

  constructor(private activatedRoute: ActivatedRoute,
    private localStorageService: LocalStorageService,
    private friendService: FriendService,
    private ratingService: RatingService,
    private formBuilder: FormBuilder,
    private router: Router) {
    this.createForm();
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      this.username = params['username'];
      if (this.username === this.localStorageService.getItem('username')) {
        this.redirectToHome();
      }

      this.checkIfFriend();
      //this.checkHasRating();
    });
  }

  redirectToHome() {
    this.router.navigate(['/home']);
  }

  checkIfFriend() {
    this.friendService.isFriend(this.username).subscribe(succ => {
    }, err => {
      this.redirectToHome();
    });
  }

  checkHasRating() {
    this.ratingService.isRated(this.username).subscribe(succ => {
    }, err => {
      this.redirectToHome();
    });
  }

  createForm() {
    this.rateForm = this.formBuilder.group({
      solo: 0,
      skill: 0,
      speech: 0,
      spirit: 0,
      sophistication: 0
    });
  }

  ratePlayer() {
    const rating = new Rating();
    rating.solo = this.rateForm.controls.solo.value;
    rating.skill = this.rateForm.controls.skill.value;
    rating.speech = this.rateForm.controls.speech.value;
    rating.spirit = this.rateForm.controls.spirit.value;
    rating.sophistication = this.rateForm.controls.sophistication.value;

    this.ratingService.ratePlayer(this.username, rating).subscribe(res => {
      this.router.navigate(['/user', this.username]);
    }, err => {
      this.router.navigate(['/oops']);
    });
  }
}
