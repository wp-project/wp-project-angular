import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user/user.service';
import {Router} from '@angular/router';
import {LocalStorageService} from '../../services/local-storage/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private userForm: FormGroup;
  private error = false;

  constructor(private localStorageService: LocalStorageService,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  login() {
    this.userService.attemptLogin(this.userForm.get('username').value, this.userForm.get('password').value)
      .subscribe(response => {
        this.error = false;
        this.localStorageService.setItem('username', '');
        this.userService.getAuthenticatedPlayer().subscribe(next => {
          this.localStorageService.setItem('username', next.username);
          this.localStorageService.setItem('nickname', next.nickname);
          this.localStorageService.setItem('hasCompletedQuiz', next.hasCompletedQuiz + '');
          if (next.hasCompletedQuiz === true) {
            this.router.navigate(['/home']);
          } else {
            this.router.navigate(['/quiz']);
          }
        });
      }, err => {
        this.error = true;
      });
  }

  private createForm() {
    this.userForm = this.formBuilder.group({
      username: ['', [Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20),
        Validators.pattern('^[a-zA-Z0-9]*$')]],
      password: ['', [Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20),
        Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.,$!@#])/)]]
    });
  }
}
