import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {LocalStorageService} from '../../services/local-storage/local-storage.service';

@Injectable()
export class QuizGuard implements CanActivateChild {
  constructor(private router: Router,
              private localStorageService: LocalStorageService) {
  }

  canActivateChild(next: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.localStorageService.hasItem('hasCompletedQuiz') &&
      this.localStorageService.getItem('hasCompletedQuiz') === 'true') {
      this.router.navigate(['/home']);
      return false;
    }
    return true;
  }
}
