import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {LocalStorageService} from '../../services/local-storage/local-storage.service';

@Injectable()
export class AuthGuard implements CanActivateChild {
  constructor(private localStorage: LocalStorageService,
              private router: Router) {
  }

  canActivateChild(next: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.localStorage.hasItem('username')) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
