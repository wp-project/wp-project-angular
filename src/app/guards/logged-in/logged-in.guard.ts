import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {LocalStorageService} from '../../services/local-storage/local-storage.service';

@Injectable()
export class LoggedInGuard implements CanActivateChild {
  constructor(private localStorageService: LocalStorageService,
              private router: Router) {
  }

  canActivateChild(next: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.localStorageService.getItem('username')) {
      this.router.navigate(['/home']);
      return false;
    }
    return true;
  }
}
