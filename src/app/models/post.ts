export class Post {
    id: number;
    text: string;
    link: string;
    username: string;
    postedOn: string;
}