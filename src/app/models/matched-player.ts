export class MatchedPlayer {
    username: string;
    nickname: string;
    matching: number;
}
