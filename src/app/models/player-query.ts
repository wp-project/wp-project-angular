export class PlayerQuery {
    solo: number;
    skill: number;
    speech: number;
    spirit: number;
    sophistication: number;
    country: string;
}