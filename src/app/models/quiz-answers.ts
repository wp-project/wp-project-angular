export class QuizAnswers {
  soloAnswers: number[];
  speechAnswers: number[];
  spiritAnswers: number[];
  sophAnswers: number[];
}
