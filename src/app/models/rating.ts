export class Rating {
  solo: number;
  skill: number;
  speech: number;
  spirit: number;
  sophistication: number;
}
