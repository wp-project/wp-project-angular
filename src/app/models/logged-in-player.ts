export class LoggedInPlayer {
  username: string;
  nickname: string;
  hasCompletedQuiz: boolean;
}
