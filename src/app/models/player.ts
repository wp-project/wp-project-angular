export class Player {
  id: number;
  username: string;
  email: string;
  password: string;
  nickname: string;
  country: string;
  steamId: string;
  battleTag: string;
  originId: string;
  games: string[];
}
